package com.brightwheel.platform;

import com.brightwheel.platform.domain.MailerTypeEnum;
import com.brightwheel.platform.domain.dto.ResponseDTO;
import com.brightwheel.platform.domain.dto.request.SendEmailRequest;
import com.brightwheel.platform.service.MailerService;
import com.brightwheel.platform.service.MailerServiceFactory;
import com.brightwheel.platform.service.impl.MailGunServiceImpl;
import com.brightwheel.platform.service.impl.SendGridServiceImpl;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@SpringBootTest
public class BrightWheelPlatformApplicationTests {

    @Test
    public void mailGunUsageTest() {
        System.setProperty("mailerType", MailerTypeEnum.MAIL_GUN.getKey());

        MailGunServiceImpl mailGunService = Mockito.mock(MailGunServiceImpl.class);
        SendGridServiceImpl sendGridService = Mockito.mock(SendGridServiceImpl.class);

        MailerServiceFactory mailerServiceFactory = new MailerServiceFactory(mailGunService, sendGridService);
        MailerService mailerService = mailerServiceFactory.getService();

        mailerService.sendEmail("to@example.com", "To Name", "from@example.com", "From Name", "Subject", "Content");
        Mockito.verify(mailGunService, Mockito.times(1)).sendEmail("to@example.com", "To Name", "from@example.com", "From Name", "Subject", "Content");

    }

    @Test
    public void sendGridUsageTest() {
        System.setProperty("mailerType", MailerTypeEnum.SEND_GRID.getKey());

        MailGunServiceImpl mailGunService = Mockito.mock(MailGunServiceImpl.class);
        SendGridServiceImpl sendGridService = Mockito.mock(SendGridServiceImpl.class);

        MailerServiceFactory mailerServiceFactory = new MailerServiceFactory(mailGunService, sendGridService);
        MailerService mailerService = mailerServiceFactory.getService();

        mailerService.sendEmail("to@example.com", "To Name", "from@example.com", "From Name", "Subject", "Content");
        Mockito.verify(sendGridService, Mockito.times(1)).sendEmail("to@example.com", "To Name", "from@example.com", "From Name", "Subject", "Content");
    }

    @Test
    public void invalidInputTest() {
        // Test Empty Input
        SendEmailRequest request1 = new SendEmailRequest();
        assertThrows(IllegalArgumentException.class, request1::validateInput);

        request1.setTo("to@example.com");
        request1.setToName("To Name");
        request1.setFrom("from@example.com");
        request1.setFromName("From Name");
        request1.setSubject("Subject");
        request1.setBody("Body");
        assertDoesNotThrow(request1::validateInput);

        // Test Invalid Email Address
        SendEmailRequest request2 = new SendEmailRequest();
        request2.setToName("To Name");
        request2.setFrom("from@example.com");
        request2.setFromName("From Name");
        request2.setSubject("Subject");
        request2.setBody("Body");

        request2.setTo("helloworld");
        assertThrows(IllegalArgumentException.class, request2::validateInput);
        request2.setTo("to@example.com");

        request2.setFrom("helloworld");
        assertThrows(IllegalArgumentException.class, request2::validateInput);
        request2.setFrom("from@example.com");

    }

    @Test
    public void mailgunHappyPathTest() {
        ResponseEntity<String> responseEntity = new ResponseEntity<>(HttpStatus.ACCEPTED);
        RestTemplate restTemplateMock = Mockito.mock(RestTemplate.class);

        when(restTemplateMock.postForEntity(anyString(), any(HttpEntity.class), eq(String.class))).thenReturn(responseEntity);

        MailGunServiceImpl mailGunService = new MailGunServiceImpl("apiKey", "apiEndPoint");
        mailGunService.setRestTemplate(restTemplateMock);

        ResponseEntity<Object> mailgunResponseEntity = mailGunService.sendEmail("to@example.com", "To Name", "from@example.com", "From Name", "Subject", "Content");
        ResponseDTO mailgunBody = (ResponseDTO) mailgunResponseEntity.getBody();
        assertNotNull(mailgunBody);
        assertEquals(mailgunResponseEntity.getStatusCode(),HttpStatus.ACCEPTED);
        assertEquals(mailgunBody.getMessage(), "Message Queued [Mailgun]");
    }

    @Test
    public void sendgridHappyPathTest() {
        ResponseEntity<String> responseEntity = new ResponseEntity<>(HttpStatus.ACCEPTED);
        RestTemplate restTemplateMock = Mockito.mock(RestTemplate.class);

        when(restTemplateMock.postForEntity(anyString(), any(HttpEntity.class), eq(String.class))).thenReturn(responseEntity);

        SendGridServiceImpl sendGridService = new SendGridServiceImpl("apiKey", "apiEndPoint");
        sendGridService.setRestTemplate(restTemplateMock);

        ResponseEntity<Object> sendGridResponseEntity = sendGridService.sendEmail("to@example.com", "To Name", "from@example.com", "From Name", "Subject", "Content");
        ResponseDTO sendgridBody = (ResponseDTO) sendGridResponseEntity.getBody();
        assertNotNull(sendgridBody);
        assertEquals(sendGridResponseEntity.getStatusCode(),HttpStatus.ACCEPTED);
        assertEquals(sendgridBody.getMessage(), "Message Queued [Sendgrid]");
    }

}
