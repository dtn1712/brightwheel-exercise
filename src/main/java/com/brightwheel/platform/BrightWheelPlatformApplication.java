package com.brightwheel.platform;

import com.brightwheel.platform.domain.MailerTypeEnum;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.Namespace;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.env.AbstractEnvironment;

@SpringBootApplication
@ImportResource({
        "classpath:application-context.xml"
})
public class BrightWheelPlatformApplication {

    private static final String APPLICATION_NAME = "BrightWheel Platform Mailer";

    private static final String DEFAULT_MAILER_SERVICE = MailerTypeEnum.SEND_GRID.getKey();

    public static void main(String[] args) {

        ArgumentParser argumentParser = ArgumentParsers.newFor(APPLICATION_NAME).build()
                .defaultHelp(true);

        argumentParser.addArgument("-t", "--type").nargs("?")
                .type(String.class).setDefault(DEFAULT_MAILER_SERVICE)
                .help("Enter the mailer service ('mailgun' or 'sendgrid'). Default to " + DEFAULT_MAILER_SERVICE + ":")
                .required(false);


        Namespace namespace = argumentParser.parseArgsOrFail(args);

        String mailerType = namespace.getString("type");

        if (!MailerTypeEnum.MAILER_TYPES.contains(mailerType)) {
            System.out.println(String.format("Enter mailer type is not in the list (%s), choose the default one %s", MailerTypeEnum.MAILER_TYPES, DEFAULT_MAILER_SERVICE));
            mailerType = DEFAULT_MAILER_SERVICE;
        }

        System.setProperty("mailerType", mailerType);

        SpringApplication.run(BrightWheelPlatformApplication.class, args);
    }

}
