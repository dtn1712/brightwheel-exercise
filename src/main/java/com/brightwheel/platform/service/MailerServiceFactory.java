package com.brightwheel.platform.service;

import com.brightwheel.platform.service.impl.MailGunServiceImpl;
import com.brightwheel.platform.service.impl.SendGridServiceImpl;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

import static com.brightwheel.platform.domain.MailerTypeEnum.MAIL_GUN;
import static com.brightwheel.platform.domain.MailerTypeEnum.SEND_GRID;

public class MailerServiceFactory {

    private MailGunServiceImpl mailGunService;

    private SendGridServiceImpl sendGridService;

    public MailerServiceFactory(MailGunServiceImpl mailGunService, SendGridServiceImpl sendGridService) {
        this.mailGunService = mailGunService;
        this.sendGridService = sendGridService;
    }

    public  MailerService getService() {
        String mailerType = System.getProperty("mailerType");
        if (MAIL_GUN.getKey().equals(mailerType)) {
            return mailGunService;
        }
        return sendGridService;
    }
}
