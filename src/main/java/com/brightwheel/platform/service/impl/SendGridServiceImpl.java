package com.brightwheel.platform.service.impl;


import com.brightwheel.platform.domain.dto.EmailContentDTO;
import com.brightwheel.platform.domain.dto.EmailDTO;
import com.brightwheel.platform.domain.dto.ResponseDTO;
import com.brightwheel.platform.domain.dto.request.SendGridSendEmailRequest;
import com.brightwheel.platform.service.MailerService;
import com.brightwheel.platform.util.WebUtils;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.*;

import static com.brightwheel.platform.interfaces.Constants.SUCCESS_HTTP_STATUSES;

@Slf4j
public class SendGridServiceImpl implements MailerService {

    private static final String TEXT_PLAIN_TYPE = "text/plain";

    private String apiEndPoint;

    private MultiValueMap<String, String> headers;

    @Setter
    private RestTemplate restTemplate;

    public SendGridServiceImpl(String apiKey, String apiEndPoint) {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Type", "application/json");
        headers.add("Authorization", String.format("Bearer %s", apiKey));
        this.headers = headers;
        this.apiEndPoint = apiEndPoint;
        this.restTemplate = new RestTemplate();
    }

    @Override
    public ResponseEntity<Object> sendEmail(String to, String toName, String from, String fromName, String subject, String body) {
        log.info(String.format("Sending email by using Sendgrid from email %s to email %s", from, to));

        SendGridSendEmailRequest sendgridSendEmailRequest = constructPayload(to, from, subject, body);
        HttpEntity<SendGridSendEmailRequest> request = new HttpEntity<>(sendgridSendEmailRequest, headers);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity(apiEndPoint, request, String.class);
        if (SUCCESS_HTTP_STATUSES.contains(responseEntity.getStatusCode())) {
            return new ResponseEntity<>(new ResponseDTO("Message Queued [Sendgrid]"), HttpStatus.ACCEPTED);
        } else {
            throw new HttpClientErrorException(responseEntity.getStatusCode(), responseEntity.getBody() != null ? responseEntity.getBody() : "Fail to call Sendgrid API to send email");
        }
    }

    private SendGridSendEmailRequest constructPayload(String to, String from, String subject, String body) {
        SendGridSendEmailRequest sendgridSendEmailRequest = new SendGridSendEmailRequest();
        sendgridSendEmailRequest.setPersonalizations(constructEmailPersonalization(to));
        sendgridSendEmailRequest.setFrom(new EmailDTO(from));
        sendgridSendEmailRequest.setSubject(subject);
        sendgridSendEmailRequest.setContent(constructEmailContent(body));

        return sendgridSendEmailRequest;

    }

    private List<Map<String, List<EmailDTO>>> constructEmailPersonalization(String to) {
        Map<String, List<EmailDTO>> map = new HashMap<>();
        map.put("to", Collections.singletonList(new EmailDTO(to)));
        return Collections.singletonList(map);
    }

    private List<EmailContentDTO> constructEmailContent(String body) {
        return Collections.singletonList(new EmailContentDTO(TEXT_PLAIN_TYPE, WebUtils.cleanHtmlContent(body)));
    }
}
