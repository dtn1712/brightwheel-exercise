package com.brightwheel.platform.service.impl;


import com.brightwheel.platform.domain.dto.ResponseDTO;
import com.brightwheel.platform.service.MailerService;
import com.brightwheel.platform.util.WebUtils;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static com.brightwheel.platform.interfaces.Constants.SUCCESS_HTTP_STATUSES;

@Slf4j
public class MailGunServiceImpl implements MailerService {

    private String apiEndPoint;

    @Setter
    private RestTemplate restTemplate;

    public MailGunServiceImpl(String apiKey, String apiEndPoint) {
        this.apiEndPoint = apiEndPoint;
        this.restTemplate = new RestTemplateBuilder().basicAuthentication("api", apiKey).build();
    }

    @Override
    public ResponseEntity<Object> sendEmail(String to, String toName, String from, String fromName, String subject, String body) {
        log.info(String.format("Sending email by using Mailgun from email %s to email %s", from, to));

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, String> payload = constructPayload(to, toName, from, fromName, subject, body);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(payload, headers);

        restTemplate.postForEntity(apiEndPoint, request, String.class);
        return new ResponseEntity<>(new ResponseDTO("Message Queued [Mailgun]"), HttpStatus.ACCEPTED);
    }

    private MultiValueMap<String, String> constructPayload(String to, String toName, String from, String fromName, String subject, String body) {
        MultiValueMap<String, String> payload = new LinkedMultiValueMap<>();
        payload.add("from", String.format("%s <%s>", fromName, from));
        payload.add("to", String.format("%s <%s>", toName, to));
        payload.add("subject", subject);
        payload.add("text", WebUtils.cleanHtmlContent(body));
        return payload;
    }
}
