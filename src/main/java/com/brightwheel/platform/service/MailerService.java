package com.brightwheel.platform.service;

import org.springframework.http.ResponseEntity;

public interface MailerService {

    ResponseEntity<Object> sendEmail(String to, String toName, String from, String fromName, String subject, String body);

}
