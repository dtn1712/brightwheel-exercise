package com.brightwheel.platform.domain.dto.request;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.util.Assert;


@Setter
public class SendEmailRequest {

    @Getter
    private String to;

    private String to_name;

    @Getter
    private String from;

    private String from_name;

    @Getter
    private String subject;

    @Getter
    private String body;

    public void validateInput() {
        Assert.notNull(to, "to cannot be null");
        Assert.notNull(to_name, "to_name cannot be null");
        Assert.notNull(from, "from cannot be null");
        Assert.notNull(from_name, "from_name cannot be null");
        Assert.notNull(subject, "subject cannot be null");
        Assert.notNull(body, "body cannot be null");

        if (!EmailValidator.getInstance().isValid(to)) {
            throw new IllegalArgumentException("to email address is invalid");
        }

        if (!EmailValidator.getInstance().isValid(from)) {
            throw new IllegalArgumentException("from email address is invalid");
        }
    }

    public String getToName() {
        return to_name;
    }

    public String getFromName() {
        return from_name;
    }

    public void setToName(String to_name) {
        this.to_name = to_name;
    }

    public void setFromName(String from_name) {
        this.from_name = from_name;
    }
}
