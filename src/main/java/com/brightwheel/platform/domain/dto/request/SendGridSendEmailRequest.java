package com.brightwheel.platform.domain.dto.request;

import com.brightwheel.platform.domain.dto.EmailContentDTO;
import com.brightwheel.platform.domain.dto.EmailDTO;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
public class SendGridSendEmailRequest {

    private List<Map<String, List<EmailDTO>>> personalizations;
    private EmailDTO from;
    private String subject;
    private List<EmailContentDTO> content;

}
