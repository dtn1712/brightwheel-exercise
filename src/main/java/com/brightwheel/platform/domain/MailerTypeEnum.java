package com.brightwheel.platform.domain;

import com.google.common.collect.ImmutableList;

import java.util.List;

public enum MailerTypeEnum {

    MAIL_GUN("mailgun"),
    SEND_GRID("sendgrid");


    public static final List<String> MAILER_TYPES = ImmutableList.of(MAIL_GUN.getKey(), SEND_GRID.getKey());

    private String key;

    MailerTypeEnum(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
