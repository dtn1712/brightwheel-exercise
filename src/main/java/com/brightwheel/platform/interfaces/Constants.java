package com.brightwheel.platform.interfaces;

import com.google.common.collect.ImmutableList;
import org.springframework.http.HttpStatus;

import java.util.List;

public class Constants {

    public static final List<HttpStatus> SUCCESS_HTTP_STATUSES = ImmutableList.of(HttpStatus.OK, HttpStatus.CREATED, HttpStatus.ACCEPTED);
}
