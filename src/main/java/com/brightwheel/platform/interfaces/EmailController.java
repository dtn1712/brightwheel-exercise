package com.brightwheel.platform.interfaces;

import com.brightwheel.platform.domain.dto.request.SendEmailRequest;
import com.brightwheel.platform.service.MailerService;
import com.brightwheel.platform.service.MailerServiceFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.Resource;
import javax.validation.Valid;

@Slf4j
@RestController
public class EmailController {

    @Resource
    private MailerServiceFactory mailerServiceFactory;

    @RequestMapping(value = "/api/v1/email/send", method = RequestMethod.POST)
    public ResponseEntity<Object> sendEmail(@Valid @RequestBody SendEmailRequest request) {
        try {
            request.validateInput();

            MailerService mailerService = mailerServiceFactory.getService();
            return mailerService.sendEmail(request.getTo(), request.getToName(), request.getFrom(), request.getFromName(), request.getSubject(), request.getBody());
        } catch (IllegalArgumentException e) {
            log.error("[IllegalArgumentException] input is invalid", e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (HttpClientErrorException e) {
            log.error("[HttpClientErrorException] calling API exception", e);
            throw new ResponseStatusException(e.getStatusCode(), e.getMessage());
        } catch (Exception e) {
            log.error(String.format("[%s] internal server error", e.getClass().getName()), e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

}
