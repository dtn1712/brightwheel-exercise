package com.brightwheel.platform.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;

public class WebUtils {

    public static String cleanHtmlContent(String htmlContent) {
        Document jSoup = Jsoup.parse(htmlContent);
        jSoup.outputSettings(new Document.OutputSettings().prettyPrint(false));
        jSoup.select("br").after("\\n");
        jSoup.select("p").before("\\n");
        String str = jSoup.html().replaceAll("\\\\n", "\n");
        return Jsoup.clean(str, "", Whitelist.none(), new Document.OutputSettings().prettyPrint(false));
    }
}
