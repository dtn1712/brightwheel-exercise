## NOTE

## Run the app

The final jar application is brightwheel-mailer-service-1.0.0.jar. Please run the command below to run the app. 
The app default mailer is SendGrid. To switch to MailGun, just add --type mailgun as below

Run using SendGrid
### `java -jar brightwheel-mailer-service-1.0.0.jar`

Run using MailGun
### `javar -jar brightwheel-mailer-service-1.0.0.jar --type mailgun`

Note that MailGun service is limited to sandbox testing only and need to have authorized recipient email specified to be able to receive email.
Please share with me the email so that I can add to the authorized recipient email in MailGun

## API Usage 

API Endpoint is /api/v1/email/send. Below is cUrl example

### `curl --header "Content-Type: application/json" --request POST  --data '{"to":"to@example.com", "to_name": "To Name", "from": "from@example.com", "from_name": "From Name", "subject": "Subject", "body": "Body"}' http://localhost:8000/api/v1/email/send`

Success response will return 202 status code and message indicate which mailer service was used. Fail response will have status code and message explain what's going wrong.

## Technology Information

The project was developed in Java using Spring Boot. The project also use Maven as build tool and dependency management.
It also use commons-validator to validate email address and jsoup to clean html tags. For more detail about dependency, please check pom.xml

### Decision

I choose Java and Spring framework for several reasons:
- Java is a strong OOP language. It give us flexiblity and power to implement design pattern and any business requirement  
- Java have a lot of library that can fulfil the implementation requirement
- Spring framework have bean config injection. This is very useful as it decouple the implementation logic with the parameter config. You can easily change the apiKey or apiEndPoint by just changing the xml bean config and don't have to make any change on code
- Spring framework is fast and extremely easy to work with. 
- Java is a proven technology for production as all major company such as Amazon, Google use Java for their service. Its performance is also better than some scripting languages such as Python, Javascript, or Ruby on Rail. It's also easier to scale up
- For build tools, Maven and Gradle is the most two powerful build tool for Java, they are both comparable. Gradle is more concise. I choose Maven as I'm more familiar with it

### Trade Off

- Java language is more verbose compare to Python, Ruby or Javascript. Therefore, the Java implementation might be slower compare to Python or Javascript
- Spring framework is also a heavier package compare to Python Flask or NodeJS. Therefore, setup and deployment will take more time compare to Python Flask or NodeJS (Real production setup for Java should use Apache Tomcat)
- If I can spend additional time on the project, I will setup Docker to make the application portable to anywhere and deployment will be easier. Also, I will use Tomcat as web server and use war package instead of jar file. I will also have the database and make the mail service type saved into the database so if we want to change the mail service, we can just update the database value without redeploying the service. I will also make the system more intelligent by trying one service first and if that service fail, it will try the other one. 


## Testing

There is unit tests written in folder src/test/java

